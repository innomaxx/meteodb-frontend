import React, {Component} from 'react';

//Copyright and footer
export default class Footer extends Component {
  render() {
    return (<footer>
      <div className="footer  mt-5">
        <p className="cp mx-auto text-center">
          <a href="http://t.me/maxrev">MaxRev</a>
          © 2018</p>
      </div>
    </footer>)
  }
}
